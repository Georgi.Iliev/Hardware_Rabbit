#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
#include <stdio.h>
#include <stdlib.h>

#include <MCP3008.h>
//#include <Adafruit_MCP3008.h>
#include <SPI.h>

#include <dht.h>
#include "dht.h"

#define vibSensor 5

dht DHT;

WiFiClient espClient;
PubSubClient client(espClient);
 
MCP3008 adc(D5, D7, D6, D8);

const char* ssid = "networkg";
const char* password =  "12345678";
const char* mqttServer = "m23.cloudmqtt.com";
const int mqttPort = 15154;
const char* mqttUser = "gyvuzoct";
const char* mqttPassword = "rq9RvwN5aQmk";

int gasSensor;
int soundSensor;
//long vibSensor;
int humSensor;

int i = 1;
char buffer [4];


void setup() {
 
  Serial.begin(115200);
  pinMode( 0, INPUT);
  pinMode(vibSensor , INPUT);
}
 
void callback(char* topic, byte* payload, unsigned int length) {
  
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
 
  Serial.println();
  Serial.println("-----------------------");
}


long get_vibration(){
 
  delay(10);
  long measurement = pulseIn (vibSensor, HIGH);  //wait for the pin to get HIGH and returns measurement
  
  return measurement;
}


void loop() {
 
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
  
    delay(500);
    Serial.println("Connecting to WiFi..");
    delay(10);
  }
  
  Serial.println("Connected to the WiFi network");
  delay(10);
  
  client.setServer(mqttServer, mqttPort);
  delay(10);
  
  client.setCallback(callback);
  delay(10);
  
  while (!client.connected()) {
  
    Serial.println("Connecting to MQTT...");
    delay(10); 
    
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
      
      Serial.println("connected");  
      delay(10);
    } 
    else {
      
      Serial.print("failed with state ");
      delay(10);
      Serial.print(client.state());
      delay(200);
    }
  }  

  //reading analog value from MQ-135 Gas Sensor (ch0) 
  gasSensor = int(adc.readADC(0));     
  delay(100);

  //reading analog value from KY-038 Sound Sensor (ch1) 
  soundSensor = int(adc.readADC(1));   
  delay(100);

  //Convert sound value to dB
  int dBValue = 20 * log10(soundSensor);
  Serial.print("Sound Sensor value in dB = ");
  Serial.print(dBValue);
  Serial.println(" dB");

  //Get vibration value by calling get_vibration method
  int measurement = get_vibration();
  delay(50);
  
  //Get current temperature and humidity values
  DHT.read11(2);

  Serial.print("Current humidity = ");
  Serial.println(DHT.humidity);
  Serial.print("Current Temperature = ");
  Serial.println(DHT.temperature);

  delay(200);
 
  //Store humidity value 
  int hum = DHT.humidity;
   
  //Store temperature value 
  int temp = DHT.temperature;

 
  //Converting the integer value received from the MQ-135 gas sensor to char and publishing to cloudMQTT
  const char* v = itoa(gasSensor, buffer, 10);  /*assig var x to the value return from the analog read //  also using a c++ function from stdio and stdlib libraries to convert the result to a const char required by the publish function from PubSubClient*/
  delay(1000);
  client.publish("Sensor/Gas", v );   /*publishing a topic with a value to mqtt cloud via built in function from PubSubClient library*/ 
  client.subscribe("Sensor/Gas");   /*subscribing to the published topic via built in function from PubSubClient library*/
  delay(1000);

  //Converting the integer value received from the KY-038 sound sensor to char and publishing to cloudMQTT
  const char* w = itoa(dBValue, buffer, 10);
  delay(1000);
  client.publish("Sensor/Sound" , w);   /*publishing a topic with a value to mqtt cloud via built in function from PubSubClient library*/ 
  client.subscribe("Sensor/Sound");
  delay(1000);
      

  //Converting the humidity integer value to char and publishing to cloudMQTT
  const char* x = itoa(hum, buffer, 10);  /*assig var x to the value return from the analog read //  also using a c++ function from stdio and stdlib libraries to convert the result to a const char required by the publish function from PubSubClient*/
  delay(1000);
  client.publish("Sensor/Humidity", x );   /*publishing a topic with a value to mqtt cloud via built in function from PubSubClient library*/ 
  client.subscribe("Sensor/Humidity");   /*subscribing to the published topic via built in function from PubSubClient library*/
  delay(1000);

  //Converting the temperature integer value to char and publishing to cloudMQTT
  const char* y = itoa(temp, buffer, 10);  /*assig var x to the value return from the analog read //  also using a c++ function from stdio and stdlib libraries to convert the result to a const char required by the publish function from PubSubClient*/
  delay(1000);
  client.publish("Sensor/Temperature", y );   /*publishing a topic with a value to mqtt cloud via built in function from PubSubClient library*/ 
  client.subscribe("Sensor/Temperature");   /*subscribing to the published topic via built in function from PubSubClient library*/
  delay(1000);

  
  //Converting the integer value received from the SW-420 vibration sensor to char and publishing to cloudMQTT
  const char* z = itoa(measurement, buffer, 10);  /*assig var x to the value return from the analog read //  also using a c++ function from stdio and stdlib libraries to convert the result to a const char required by the publish function from PubSubClient*/
  delay(1000);
  client.publish("Sensor/Vibration", z );   /*publishing a topic with a value to mqtt cloud via built in function from PubSubClient library*/ 
  client.subscribe("Sensor/Vibration");   /*subscribing to the published topic via built in function from PubSubClient library*/
  delay(1000);
}
   


